//
// Created by JinTian on 26/07/2017.
//

#ifndef LEETCODE_SOLUTION_H
#define LEETCODE_SOLUTION_H

#include <iostream>
#include <vector>
#include <string>

using namespace std;


class Solution{
public:
    string authorName;
    string task;
    string time;

    Solution ();
    ~Solution();

    // many method for solution
    std::vector<int> twoSum(std::vector<int>& nums, int target);
    int lengthOfLongestSubstring(string s);

};



#endif //LEETCODE_SOLUTION_H
