//
// Created by JinTian on 26/07/2017.
//

#ifndef _T_PRINT_H
#define _T_PRINT_H


#include <iostream>
#include <vector>
#include <string>

using namespace std;

template <typename  T> void print_vector_bug(const vector<T>& v) ;


void print_test(int t);
//void print_vector(auto* p_start, auto* p_end);

template <typename  T> T random_element(const std::vector<T>& options);

#endif //_T_PRINT_H
