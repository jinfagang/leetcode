//
// Created by JinTian on 26/07/2017.
//

#include "t_sort.h"
#include "t_print.h"

using namespace std;


void quick_atom(vector<int> &a, int left_index, int right_index) {
    // Atom operation to divide a into seg1 and seg2
    if (left_index <right_index) {
    int currentValue = a[left_index];
    int i = left_index;
    int j = right_index;
    while (i != j) {
        while (i < j && a[j] >currentValue)j--;
        if(i<j)a[i++]=a[j];
        while (i < j && a[i] <currentValue)i++;
        if(i<j)a[j--]=a[i];
    }
        a[i]=currentValue;
    quick_atom(a, left_index, i - 1);
    quick_atom(a, i + 1, right_index);
}
}

vector<int> quick_sort(vector<int> &a, bool max_first) {
    max_first = true;

    if (a.size() == 1) {
        return a;
    } else {
        quick_atom(a, 0, (int) a.size());
        return a;
    }
}
