//
// Created by JinTian on 26/07/2017.
//

#ifndef _T_SORT_H
#define _T_SORT_H


#include <iostream>
#include <vector>
using namespace std;

vector<int> quick_sort(vector<int> &a, bool max_first);

#endif //_T_SORT_H
